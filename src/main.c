/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "lcd.h"
#include "stdio.h"
#include "math.h"
#include "stdlib.h"
#include "time.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim2;

/* USER CODE BEGIN PV */
int game =0;
int row;
extern int count;
extern int speed;
char game_array[2][16];
int write = 0;
int space;
int score;
int life;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM2_Init(void);
/* USER CODE BEGIN PFP */
/** this function a initialized game array start values for user can start game*/
void init_game_array(char game_array[2][16]);
/** this function check a continue game or player are lose. Change location player
"car" and dicrease count life if user lose
row - is value where must be "car" depending on the angle*/
int games(char game_array[2][16],int row);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_TIM2_Init();
  /* USER CODE BEGIN 2 */
  srand(time(0));
  /** configurate pinout for work with lcd
	A3 - d4
	A2 - d5
	A1 - d6
	A0 - d7
  */
  Lcd_PortType ports[] = {
    		GPIOA, GPIOA, GPIOA, GPIOA };

    Lcd_PinType pins[] = { GPIO_PIN_3, GPIO_PIN_2, GPIO_PIN_1, GPIO_PIN_0 };
  /** create lcd with configuration pin for data and
	A8 - rs
	A9 - e
	A10 - rw (default is 0 and don't change, only write)
  */
    lcd = Lcd_create(ports, pins, GPIOA, GPIO_PIN_8, GPIOA, GPIO_PIN_9, LCD_4_BIT_MODE);
    Lcd_string(&lcd, "Press button to");
    Lcd_cursor(&lcd, 1, 6);
    Lcd_string(&lcd, "start");	
  /** write 0x07 into acelerometer in order to enable work ADC */
    int res = HAL_I2C_Mem_Write(&hi2c1, 0x68<<1, 0x6B, I2C_MEMADD_SIZE_8BIT, 0x07, 1, 1000) ;
    if(res>0){
    	Lcd_string(&lcd, "Problem with accelerometer");
    }
  /** read register "Who am I" and if res or rd(adress acelerometer) is not correct show it on lcd*/
    res = HAL_I2C_Mem_Read(&hi2c1, 0x68<<1, 0x75,I2C_MEMADD_SIZE_8BIT, &rd, 1, 1000);
    if(res>0){
        	Lcd_string(&lcd, "Problem with accelerometer");
    }
    if(rd != 104)
    	Lcd_string(&lcd, "Not correct address");
    init_game_array(game_array);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
int8_t buffer[2];
	  int16_t buffero;
	  if(game){
		/** show game_array for user*/	
		  Lcd_cursor(&lcd, 0, 0);
		  Lcd_string(&lcd, &game_array[0][0]);
		  Lcd_cursor(&lcd, 1, 0);
		  Lcd_string(&lcd, &game_array[1][0]);
	  		  if(write){
				/** get value of ADC from acelerometer*/
	  			  for(int i=0;i<3;i++){
	  			res = HAL_I2C_Mem_Read(&hi2c1, 0x68<<1, 0x3D,I2C_MEMADD_SIZE_8BIT, &buffer[0], 1, 1000);
	  			res = HAL_I2C_Mem_Read(&hi2c1, 0x68<<1, 0x3E,I2C_MEMADD_SIZE_8BIT, &buffer[1], 1, 1000);
	  			int16_t	tmp_buffero = ((buffer[0] << 8) | buffer[1]);
	  			if(abs(buffero)<abs(tmp_buffero)){
	  				buffero = tmp_buffero;
	  			}
	  			  }
				/** calculate value of pitch*/
	  			if(res == 0){
	  					float ay = buffero/65536.0;
	  					buffero=0;
	  					float synch = 57.295;
	  					float pitch;
	  					if( ay >= 0){
	  					            pitch = 90 - synch*acos(ay);
	  					        } else {
	  					            pitch = synch*acos(-ay) - 90;
	  					        }
	  					pitch*=6.5;
					/** say what choose of location "car" did the user*/ 
	  				if(pitch>20){
	  					if(row+1!=2){
	  					if(games(game_array, row+1)){
	  						Lcd_cursor(&lcd, 0, 0);
	  						Lcd_string(&lcd, &game_array[0][0]);
	  						Lcd_cursor(&lcd, 1, 0);
	  						Lcd_string(&lcd, &game_array[1][0]);
	  						row++;
	  					}
	  					}
	  				}else if(pitch<-20){
	  					if(row-1!=-1){
	  						if(games(game_array, row-1)){
	  							Lcd_cursor(&lcd, 0, 0);
	  							Lcd_string(&lcd, &game_array[0][0]);
	  							Lcd_cursor(&lcd, 1, 0);
	  							Lcd_string(&lcd, &game_array[1][0]);
	  							row--;
	  						}
	  					}
	  				}else{
	  					if(games(game_array, row)){
	  						Lcd_cursor(&lcd, 0, 0);
	  						Lcd_string(&lcd, &game_array[0][0]);
	  						Lcd_cursor(&lcd, 1, 0);
	  						Lcd_string(&lcd, &game_array[1][0]);
	  					}
	  				}
	  			}
	  		  }
	  		  write= 0;
	  	  }else{
			/** show countdown for user*/
	  		  if(count > 0){
	  		  if(write){
	  			  Lcd_clear(&lcd);
	  			  Lcd_cursor(&lcd, 0, 8);
	  			  Lcd_int(&lcd, count);
	  			  write = 0;
	  		  }
	  		  }else{
				/** start game*/
	  			  game = 1;
	  			  //Lcd_cursor(&lcd, 0, 0);
	  			  //Lcd_string(&lcd, "Change the display orientation!!");
	  			  write = 0;
	  		  }
	  	  }
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 8000;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 1000;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */
  HAL_TIM_Base_Start_IT(&htim2);
  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3 
                          |GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10, GPIO_PIN_RESET);

  /*Configure GPIO pins : PA0 PA1 PA2 PA3 
                           PA8 PA9 PA10 */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3 
                          |GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : PB10 */
  GPIO_InitStruct.Pin = GPIO_PIN_10;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 1, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */
void init_game_array(char game_array[2][16]){
	game_array[0][0] = ' ';
	game_array[1][0] = '#';
	game_array[0][15]=life+48;
	game_array[1][15]=speed+48;
	for(int i = 1;i < 15;i+=2){
		int tmp = rand()%2;
		game_array[0][i] = ' ';
		game_array[1][i] = ' ';
		game_array[tmp][i+1] = ' ';
		tmp = (tmp + 1)%2;
		game_array[tmp][i+1] = '*';
	}
	space = 1;
	row = 1;
	life = 3;
	score = 0;
}
int games(char game_array[2][16],int row){
	if(game_array[row][1]=='*'){
		if(life - 1 == 0){
		Lcd_clear(&lcd);
		Lcd_cursor(&lcd, 0, 0);
		Lcd_string(&lcd, "You lose.");
		HAL_Delay(500);
		Lcd_clear(&lcd);
		Lcd_cursor(&lcd, 0, 2);
		Lcd_string(&lcd, "Your score:");
		Lcd_cursor(&lcd, 1, 6);
		Lcd_int(&lcd, score);
		init_game_array(game_array);
		game = 0;
		count= 6;
		speed = 3;
		return 0;
		}else
		life--;
	}
		for(int i=0;i<15;i++){
			game_array[0][i] = game_array[0][i+1];
			game_array[1][i] = game_array[1][i+1];
		}
		if(space){
			game_array[0][14] = ' ';
			game_array[1][14] = ' ';
			space--;
		}
		else{
			int tmp = rand()%2;
			game_array[tmp][14] = '*';
			tmp = (tmp + 1)%2;
			game_array[tmp][14] = ' ';
			space++;
		}
		game_array[row][0] = '#';
		game_array[0][15] = life+48;
		game_array[1][15] = speed+48;
		return 1;
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
